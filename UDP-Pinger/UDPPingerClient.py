# UDPPingerClient.py
# Let's send some UDP guys
#
# Kathryn Blecher
# 108871623
# This program pings a server 10 times and outputs the time, 
# return time, and time out information
#
#
# get the IP of current location
import socket
ip = socket.gethostbyname(socket.getfqdn())
# import necessary components
from socket import *
from datetime import datetime, timedelta
# set up port connection
serverPort = 6623
clientSocket = socket(AF_INET, SOCK_DGRAM)
clientSocket.connect((ip, serverPort))
# give us something to send to test if it works
sentence = 'ping'
# set timeout to 1 second
clientSocket.settimeout(1)
# lets try 10 pings to socks
counter = 10
ping_number = 1
while ping_number <= counter:
    # send ping
    time = datetime.now()
    clientSocket.send(sentence)
    print 'Ping ', ping_number, '\n'
    try:
        # we will print reply
        modifiedSentence = clientSocket.recv(1024)
        rt_time = datetime.now()
        rtt = rt_time - time
        print 'From Server: ', modifiedSentence, ' ', time.time()
        print 'RTT: ', rtt.total_seconds(), '\n'
    except timeout:
        # we timed out due to poor server
        print 'Request timed out\n'
    ping_number+=1
clientSocket.close()
